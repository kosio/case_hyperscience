import os

def get_full_list_of_files(base_folder, filter_files):
    list_of_files = []
    for (dirpath, dirnames, filenames) in os.walk(base_folder):
        fs = [os.path.join(dirpath, fn) for fn in filenames if fn.endswith(filter_files)]
        list_of_files.extend(fs)

    return list_of_files
