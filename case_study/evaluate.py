import difflib
import codecs
import os.path
import random
from helpers import get_full_list_of_files

def sequ(s1, s2):
    words1 = s1.split()
    words2 = s2.split()
    matcher = difflib.SequenceMatcher(a=words1, b=words2)
    for block in matcher.get_matching_blocks():
        if block.size == 0:
            continue
        yield ' '.join(words1[block.a:block.a+block.size])

def evaluate_ocr_against_originals_with_rotations(fnocr, originals):
    base_result = evaluate_ocr_against_originals(fnocr, originals)
    fnocr_rotated_list = fnocr.split(os.sep)
    fnocr_rotated_list[-2] = "rotated"
    fnocr_rotated = os.sep.join(fnocr_rotated_list)
    if os.path.isfile(fnocr_rotated):
        rotated_result = evaluate_ocr_against_originals(fnocr_rotated, originals)
        merged_results = base_result+rotated_result
        sort_results(merged_results)
        return merged_results[:2]
    else:
        return base_result

def sort_results(results):
    results.sort(key=lambda x: (x[1], x[2], x[1] / float(x[4] + 1)), reverse=True)


def evaluate_ocr_against_originals(fnocr, originals):
    s1 = codecs.open(fnocr, encoding="utf-8").read()
    results = []
    for f in originals:
        s2 = originals[f]
        similarity_list = list(sequ(s1, s2))
        similarity_result = (
            len(similarity_list),
            len("\t".join(similarity_list)),
            max(list(map(len,similarity_list))+[0]),
            len(s1),
            len(s2),
            f
        )
        # print(similarity_result)
        results.append(similarity_result)

    results.sort(key=lambda x: (x[1],x[2], x[1]/float(x[4]+1)), reverse=True)
    #print(results[:5])

    return results[:2]

def load_originals():
    bf = "c:\\projects\\hs\\datathon_dataset\\_original_forms_empty\\"
    list_of_original_files = get_full_list_of_files(bf, '.txt')
    originals = {}
    for f in list_of_original_files:
        originals[f] = codecs.open(
            os.path.join(bf, f),
            encoding="utf-8",
            errors='ignore').read()
    return originals

def predict_results(originals, file_list):
    results = {}
    c = 0
    for fnocr in file_list:

        results[fnocr] = evaluate_ocr_against_originals_with_rotations(fnocr, originals)
        print(results[fnocr])
        c += 1
        if c % 200 == 0:
            print(c)
    return results

def get_form_page_from_train_file(f_name):
    guess = f_name[:-8].split(os.sep)[-1].split("__")
    del guess[1]
    return guess

def get_prediction_from_results(result):
    return result[-1][:-4].split(os.sep)[-1].split("__")

def calculate_train_score(results):
    total_result_form = 0
    total_result_exact = 0
    for f_name in results:
        correct = get_form_page_from_train_file(f_name)
        answer = get_prediction_from_results(results[f_name][0])
        result_form = correct[0] == answer[0]
        result_exact = correct[0] == answer[0] and correct[1] == answer[1]
        total_result_form += result_form
        total_result_exact += result_exact

        if not result_exact:
            print(f_name, results[f_name])
    return (total_result_form, total_result_exact)

def calculate_test_prediction(results, prediction_output_file):
    fw = open(prediction_output_file,"w")
    fw.write(",".join([
        "prediction_id",
        "predicted_form",
        "predicted_page",
        "n_cse",
        "total_length_cse",
        "max_individual_length_cse",
        "ocr_length_sample",
        "ocr_length_predicted_form"
    ])+"\n")
    fww = open(prediction_output_file+".rotate","w")
    for f_name in results:
        print(results[f_name])
        answer = get_prediction_from_results(results[f_name][0])
        prediction_id = f_name[:-8].split(os.sep)[-1]
        fw.write(",".join([prediction_id]+answer+list(map(str,results[f_name][0])))+"\n")
        if results[f_name][0][1] <= 100 or results[f_name][0][2] <= 20:
                fww.write(prediction_id+"\n")
    fww.close()
    fw.close()

def train(sample_size):
    originals = load_originals()
    bfocr = "/home/ubuntu/hs_ocr/train/"
    # bfocr = "c:\\projects\\hs_ocr\\test\\"
    list_of_ocr_files = get_full_list_of_files(bfocr, '.txt')

    file_list = random.sample(list_of_ocr_files, sample_size)
    results = predict_results(originals, file_list)
    print(calculate_train_score(results))

def test():
    originals = load_originals()
    bfocr = "c:\\projects\\hs_ocr\\test\\"
    list_of_ocr_files = get_full_list_of_files(bfocr, '.txt')
    print(len(list_of_ocr_files))
    #list_of_ocr_files = random.sample(list_of_ocr_files, 100)
    list_of_ocr_files = list(filter(lambda x: "9e20fdef-d2cd-42af-967e-8b71f832951c.png.txt" in x, list_of_ocr_files))
    print(list_of_ocr_files)
    results = predict_results(originals, list_of_ocr_files)
    calculate_test_prediction(results, "predictions_simple.csv")


# Thanslate originals into text
#  for fn in `ls *.pdf`; do for i in `seq 1 20`; do pdftotext -f $i -l $i $fn $fn\_\_$i.txt; done done

# Rotate images
# for file in *.png; do convert $file -rotate 180 ../rotated-test-dataset/$file; done

if __name__ == "__main__":
    #train(100)
    test()
