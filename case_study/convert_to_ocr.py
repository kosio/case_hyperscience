from PIL import Image
import pytesseract
import os.path
import codecs
import random
from multiprocessing import Pool, Manager

from helpers import get_full_list_of_files

def process_ocr_text_from_file(file_name):
    #print(file_name)
    base_folder = "/home/ubuntu/rotated-test-dataset/"
    try:
        fwn = "./rotated/" + (file_name[len(base_folder):] + ".txt").replace(os.sep, "__")
        if os.path.isfile(fwn):
            return
        ocr_text = pytesseract.image_to_string(Image.open(file_name), lang='eng')
        fw = codecs.open(fwn, "w", encoding="utf-8")
        fw.write(ocr_text)
        fw.close()
    except OSError as err:
        print(file_name, err)


def annotate_with_ocr(list_of_files):
    pool = Pool(processes=8)

    pool.map(process_ocr_text_from_file, list_of_files)


if __name__ == "__main__":
    
	#base_folder = "c:\\projects\\hs\\test_dataset\\test_dataset5\\"
    #list_of_files = get_full_list_of_files(base_folder, '.png')
    
    list_of_files = list(map(lambda x: "/home/ubuntu/rotated-test-dataset/"+x+".png",filter(None, map(lambda x: x.strip(),open("predictions.csv.rotate1").readlines()))))
    print(len(list_of_files),list_of_files[0])
    
    random.shuffle(list_of_files)
    annotate_with_ocr(list_of_files)